import React from "react";

import { useNavigate } from "react-router-dom";
import { Header } from "../../styles/StudyStyles";

const HEADER = [
  {
    name: "홈",
    path: "/",
  },
  {
    name: "리스트",
    path: "/list",
  },
  {
    name: "라이브러리 리스트",
    path: "/api-list",
  },
];

const Layout = ({ children }) => {
  const navigator = useNavigate();

  const goUrl = (url) => {
    navigator(url);
  };

  return (
    <React.Fragment>
      {/* Header */}
      <Header>
        <ul className="menu-container">
          {HEADER?.map((menu) => (
            <li onClick={() => goUrl(menu?.path)}>{menu?.name}</li>
          ))}
        </ul>
      </Header>

      {/* Main */}
      <main
        style={{
          minHeight: "80vh",
        }}
      >
        {children}
      </main>

      {/* Footer */}
      <footer
        style={{
          height: "200px",
          backgroundColor: "#555555",

          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        푸터입니다.
      </footer>
    </React.Fragment>
  );
};

export default React.memo(Layout);
