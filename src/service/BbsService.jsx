import { API_URL as API } from "../constants";
import axios from "axios";

const getList = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${API}/product`).then((response) => {
      if (response?.data && response?.data?.value) {
        const val = response.data.value.sort((a, b) => (a.createdAt > b.createdAt ? -1 : 1));

        resolve({
          list: val,
          total: response.data.count > 0 ? response.data.count : response.data.total,
          cursor: response.data.cursor,
          backCursor: response.data.backCusrsor,
        });
      } else {
        resolve("");
      }
    });
  });
};

const getData = (id) => {
  return new Promise((resolve, reject) => {
    axios.get(`${API}/product/${id}`).then((response) => {
      if (response?.data?.code === 200) {
        resolve(response?.data?.value);
      }
    });
  });
};

const BbsService = {
  getList,
  getData,
};

export default BbsService;
