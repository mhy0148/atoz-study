// 모바일 변형되는 사이즈 (아이패드, 노트)
export const MOBILE_SIZE = '1000px';

// 최대 Width
export const MAX_WIDTH = '1920px';

export const MD_WIDTH = '60%';

// 패딩
export const pd_w = '0 20%';
export const pd_m = '0 5%';

// layOut
export const smLayout = `@media screen and (max-width: ${MOBILE_SIZE})`;
export const mdLayout = `@media screen and (min-width: 700px) and (max-width: ${MOBILE_SIZE})`;
export const xlLayout = `@media screen and (min-width: ${MOBILE_SIZE}) and (max-width: 1365px)`;
export const uxLayout = `@media screen and (min-width: 1921px) and (max-width: 4096px)`;
