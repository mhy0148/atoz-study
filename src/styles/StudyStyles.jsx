import styled from "styled-components";

export const Header = styled.header`
  width: 100%;
  height: 100px;
  background-color: rgb(238, 238, 238);
  display: flex;
  justify-content: center;
  align-items: center;

  padding: 0 5%;

  .menu-container {
    width: 100%;

    display: flex;
    justify-content: flex-start;
    align-items: center;

    gap: 15px;

    li {
      cursor: pointer;

      &:hover {
        color: #f33;
      }
    }
  }
`;

export const Container = styled.div`
  width: 100%;
  padding: 30px 5%;

  h3 {
    font-size: 1.1em;
    font-weight: bold;
  }

  ul {
    margin: 10px 0;

    li {
      color: #555555;
      font-size: 1em;
      margin: 3px 0;
    }
  }
`;

export const StudyContainer = styled.div`
  /* TODO.. */
`;
