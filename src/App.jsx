import React, { lazy, Suspense } from "react";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// Main Page
const MainPage = lazy(() => import("./pages/MainPage"));
const ListPage = lazy(() => import("./pages/ListPage"));
const APIListPage = lazy(() => import("./pages/APIListPage"));

function App() {
  return (
    <Router>
      <Suspense fallback={<span>Loading ....</span>}>
        <Routes>
          <Route path="/" element={<MainPage />} exact />

          <Route path="/list" element={<ListPage />} exact />

          <Route path="/api-list" element={<APIListPage />} exact />
        </Routes>
      </Suspense>
    </Router>
  );
}

export default App;
