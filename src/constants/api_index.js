import axiosInstance from './AxiosInstance';
import axios from 'axios';
import { API_URL } from './index';

// Get data..
export const getData = async item => {
  if (!item) return;
  const { data } = await axios.get(`${API_URL}/${item}`);

  // react-query 함께 사용하세요.
  return data;
};

// Set data..
export const setData = async (item, param) => {
  if (!param) return;
  let res = await axiosInstance.post(`${API_URL}/${item}`, param);

  return res;
};

// Update data..
export const UpdateData = async (item, param) => {
  if (!param) return;
  let res = await axiosInstance.put(`${API_URL}/${item}/`, param);

  return res;
};

// Delete data [Body]
export const deleteData = async (item, id) => {
  if (!item) return;
  let res = await axiosInstance.delete(`${API_URL}/${item}/${id}`);

  return res;
};

// Delete data [Param]
export const deleteParamData = async (item, param) => {
  if (!param) return;
  let res = await axiosInstance.delete(`${API_URL}/${item}?${param}`);

  return res;
};

// Delete data [Form]
export const deleteFormData = async (item, form) => {
  if (!form) return;
  let res = await axiosInstance.delete(`${API_URL}/${item}`, form);

  return res.data;
};
