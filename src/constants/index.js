import moment from "moment";

// export const API_URL = 'http://localhost:5000';
export const API_URL = "https://api.seekorea.site";

export const APP_PREFIX = "LOCAL-STAY";
export const IMG_PATH = "/assets/images";
export const S3_BUCKET = "file.travelmaster.com";
export const BASE_IMG_URL = "https://s3.ap-northeast-2.amazonaws.com/file.travelmaster.co.kr/";

/**
 * 실서버  = 반영 시 도메인 서버 주소로 변경
 * 로컬서버 = 자신 아이피 주소로 변경
 */
// export const DOMAIN = '.192.168.0.2';
// export const DOMAIN = '.192.168.1.51';
export const DOMAIN = "www.localstay.co.kr";

/**
 * 로그인 쿠키 아이디
 * 약자 ID_JTW = 토큰 값
 * 약자 ID_SES = 로그인 아이디
 * 약자 LSID_KEYWORD = 최근검색어
 */
const TOKEN = "LSID_JWT";
const USER_SESSION = "LSID_SES";
export const RECENTLY_KEYWORD = "LSID_KEYWORD";

/**
 * RECORD_MEMBER : 기록가 회원
 * PREMIUM_MEMBER : 유료 회원
 * ADMIN : 관리자
 */
export const RECORD_MEMBER = "RECORD_MEMBER";
export const PREMIUM_MEMBER = "PREMIUM_MEMBER";
export const ADMIN = "ADMIN";

export const removeStorage = () => {
  for (let [k, v] of Object.entries(localStorage)) {
    if (k !== "lang") {
      localStorage.removeItem(k);
    }
  }
  sessionStorage.clear();
};

// Mobile Check
export const isMobile = () => {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
};

export const bodyHiddenToggle = (swich) => {
  let body = document.querySelector("body");

  if (swich) {
    body.classList.add("hidden");
  } else {
    body.classList.remove("hidden");
  }
};

// 날짜 변환
export const setCreatedAt = (createdAt) => {
  if (!(createdAt > 0)) return "-";

  const dt = new Date(Number(createdAt));
  let addDt =
    dt.getFullYear() +
    "." +
    ("00" + (dt.getMonth() + 1).toString()).slice(-2) +
    "." +
    ("00" + dt.getDate().toString()).slice(-2) +
    " " +
    ("00" + dt.getHours().toString()).slice(-2) +
    ":" +
    ("00" + dt.getMinutes().toString()).slice(-2);

  return addDt.slice(0, 10);
};

// 시간 형식 변경
export const timeFormat = (seconds) => {
  if (isNaN(seconds)) {
    return `0:00`;
  }
  const date = new Date(seconds * 1000);
  const hh = date.getUTCHours();
  const mm = date.getUTCMinutes();
  const ss = date.getUTCSeconds().toString().padStart(2, "0");
  if (hh) {
    return `${hh}:${mm.toString().padStart(2, "0")}:${ss}`;
  }
  return `${mm}:${ss}`;
};

// 시간 체크
export const diffToText = (date) => {
  const _diff = moment().diff(moment(Number(date)), "days");

  if (_diff < 1) {
    const _hour = moment().diff(moment(Number(date)), "hours");
    const _minute = moment().diff(moment(Number(date)), "minutes");
    const _second = moment().diff(moment(Number(date)), "seconds");

    if (_hour < 1) {
      if (_minute < 1) {
        return Math.abs(_second ? _second : 1) + "초 전";
      } else {
        return _minute + "분 전";
      }
    } else {
      return _hour + "시간 전";
    }
  } else if (_diff < 364) {
    return moment().diff(moment(Number(date)), "days") + "일 전";
  } else {
    return moment(Number(date)).format("YYYY-MM-DD");
  }
};

export const handleNumChange = (value) => {
  value = value.replace(/[^0-9]/g, "");
  return value.replace(/(^02.{0}|^01.{1}|[0-9]{4})([0-9]+)([0-9]{2})/, "$1-$2-$3");
};

export const numberRender = (no) => {
  return no.toString().padStart(2, "0");
};

export const textSilce = (title, silce) => {
  if (!title) return;

  return title.length > silce ? `${title.slice(0, silce)}...` : title;
};

export const addComma = (num) => {
  const regexp = /\B(?=(\d{3})+(?!\d))/g;
  let _num = num + "";
  return _num.toString().replace(regexp, ",");
};

export const padNumber = (num, length) => {
  return String(num).padStart(length, "0");
};

export const regExp = (type) => {
  switch (type) {
    case "name":
      return /^[가-힣]{2,}$/g;
    case "search":
      return /^[0-9ㄱ-ㅎㅏ-ㅣ가-힣a-zA-Z]*$/g;
    case "nickname":
      return /^[0-9ㄱ-ㅎㅏ-ㅣ가-힣]{1,10}$/g;
    case "email":
      return /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
    case "password":
      return /((?=.*\d)(?=.*[a-z])(?=.*[\W]).{8,})/g;
    case "number":
      return /^[0-9]+$/;
  }
};
