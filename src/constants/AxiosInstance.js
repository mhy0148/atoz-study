import axios from 'axios';
import { Auth } from 'aws-amplify';

import { API_URL, cookieRemove, getJwt, setJwt } from './index';

import awsconfig from '../aws-exports';

Auth.configure(awsconfig);

const TOKEN = getJwt();

const axiosInstance = axios.create({
  baseURL: API_URL,
  timeout: 1000 * 30,
  headers: {
    Pragma: 'no-cache',
    CacheControl: 'no-cache',
    Expires: '0',
    usertype: 'user',
    Authorization: TOKEN,
  },
});

axiosInstance.interceptors.request.use(
  config => {
    const token = TOKEN;
    config.headers.Authorization = token;
    config.headers.usertype = 'user';
    config.headers.Bucket = awsconfig.aws_user_files_s3_bucket;
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

axiosInstance.interceptors.response.use(
  response => {
    return response;
  },

  error => {
    if (error.response) {
      console.log(error.response);
      if (error.response && error.response.status === 401) {
        tokenRefresh().then(res => {
          if (res) {
            window.location.reload();
          }
        });
      }
      return Promise.resolve(error.response.data);
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }

    return Promise.resolve(error); //reject(error)
  },
);

export const tokenRefresh = async () => {
  console.log('토큰 만료');

  try {
    const cognitoUser = await Auth.currentAuthenticatedUser();
    const currentSession = await Auth.currentSession();

    return new Promise(function (resolve, reject) {
      cognitoUser.refreshSession(currentSession.refreshToken, (err, session) => {
        if (session) {
          const { accessToken } = session;

          setJwt(accessToken.jwtToken);
          resolve(accessToken.jwtToken);
        }
      });
    });
  } catch (e) {
    console.log('Unable to refresh Token : ', e);
    if (e === 'The user is not authenticated') {
      cookieRemove();
      Auth.signOut();
    }
  }
};

export default axiosInstance;
