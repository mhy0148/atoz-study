import React, { useContext } from "react";

import Layout from "../components/layout/Layout";

import { LayoutContext } from "../context/LayoutContext";
import { Container } from "../styles/StudyStyles";

const MainPage = () => {
  const { matchese } = useContext(LayoutContext);

  return (
    <Layout>
      <Container>
        <h3>아토즈 소프트 과제</h3>

        <ul>
          <li>
            1. 상단 리스트 탭으로 이동하여 자신의 스타일로 게시판을 만드세요.
          </li>

          <li>
            2. 상단 라이브러리 리스트 탭으로 이동하여 자신의 스타일로 게시판을
            만드세요.
          </li>

          <li>
            3. 리스트의 상세 페이지를 만드세요. (직접 페이지 생성 및 라우터
            설정)
          </li>
        </ul>
      </Container>
    </Layout>
  );
};

export default MainPage;
