import React, { useContext, useEffect, useState } from "react";

import Layout from "../components/layout/Layout";

import { LayoutContext } from "../context/LayoutContext";
import { Container } from "../styles/StudyStyles";
import BbsService from "../service/BbsService";

const ListPage = () => {
  const { matchese } = useContext(LayoutContext);

  const [data, setData] = useState([]);

  useEffect(() => {
    BbsService.getList().then((res) => {
      setData(res);
    });
  }, []);

  console.log(data);

  return (
    <Layout>
      <Container>
        <p>1. 하단에 자신이 원하는 스타일로 게시판을 만들어보세요.</p>
      </Container>

      <div>{/* 여기에 작업 */}</div>
    </Layout>
  );
};

export default ListPage;
