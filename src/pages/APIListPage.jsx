import React, { useContext } from "react";

import Layout from "../components/layout/Layout";

import { LayoutContext } from "../context/LayoutContext";
import { Container } from "../styles/StudyStyles";

import { useQuery, useInfiniteQuery } from "react-query";

const APIListPage = () => {
  const { matchese } = useContext(LayoutContext);

  return (
    <Layout>
      <Container>
        <p>2. 리액트 쿼리 라이브러리를 사용하여 게시판을 만들어보세요.</p>
      </Container>

      <div>{/* 여기에 작업 */}</div>
    </Layout>
  );
};

export default APIListPage;
