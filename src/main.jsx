import "./assets/scss/style.scss";
import "./index.css";

import React from "react";
import ReactDOM from "react-dom/client";

import LayoutContextProvider from "./context/LayoutContext";
import App from "./App";

import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      // staleTime : Fresh -> Stale 상태로 변경되는 시간
      // 데이터가 한번 fetch 되고 나서 staleTime이 지나지 않았다면 unmount 후 mount 되어도 fetch가 일어나지 않는다.
      staleTime: 50000,

      // cacheTime : 데이터가 비활성 상태일 때 캐싱된 상태로 남아있는 시간
      // 쿼리 인스턴스가 unmount 되면 데이터는 inactive 상태로 변경되며, 캐시는 cacheTime만큼 유지된다.
      cacheTime: 50000,

      // refetchOnMount 는 데이터가 stale 상태일 경우 마운트 시 마다 refetch를 실행하는 옵션이다.
      refetchOnMount: true,

      // refetchOnReconnect 는 데이터가 stale 상태일 경우 재 연결 될 때 refetch를 실행하는 옵션이다.
      refetchOnReconnect: false,

      // refetchOnWindowFocus 는 데이터가 stale 상태일 경우 윈도우 포커싱 될 때 마다 refetch를 실행하는 옵션이다.
      refetchOnWindowFocus: false,

      // suspense : 호출하는 컴포넌트가 Suspense로 감쌀지 말지 정해주는 역할
      suspense: false,

      // retry : 호출 실패시 몇번 반복해서 호출 할지
      retry: 3,
    },
  },
});

const container = document.getElementById("root");

ReactDOM.createRoot(container).render(
  <QueryClientProvider client={queryClient}>
    <LayoutContextProvider>
      <App />
    </LayoutContextProvider>
  </QueryClientProvider>
);
